import React, { useEffect, useState } from "react";
import parse from "html-react-parser";
import { renderToString } from "react-dom/server";

import logo from "./logo.svg";
import "./App.css";
import { MdAccessibilityNew } from "react-icons/md";
import { MdAccessibleForward } from "react-icons/md";
import { MorphReplace } from "react-svg-morph";
import SvgA from "./SvgA";
import SvgB from "./SvgB";
import SvgTwo from "./SvgTwo";
import SvgBrush from "./SvgBrush";

function App() {
  const [checkState, setcheckState] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      setcheckState(!checkState);
    }, 1500);
  }, [checkState]);

  console.log(9999);

  return (
    <div className="App">
      <h1>Hi All</h1>
      <div>
        {" "}
        {/* {checkState ? <MdAccessibilityNew /> : <MdAccessibleForward />} */}
        <MorphReplace width={50} height={50}>
          {checkState
            ? getSVGElement(<MdAccessibilityNew fill="red" />, "playSVG")
            : getSVGElement(<MdAccessibleForward />, "pauseSVG")}
        </MorphReplace>
      </div>
      <div>
        <MorphReplace width={50} height={50}>
          {checkState
            ? getSVGElement(<SvgA fill="red" />, "bb")
            : getSVGElement(<SvgB />, "nn")}
        </MorphReplace>
      </div>
      <div>
        <MorphReplace width={50} height={50}>
          {checkState
            ? getSVGElement(<SvgTwo fill="red" />, "bb")
            : getSVGElement(<SvgBrush />, "nn")}
        </MorphReplace>
      </div>
    </div>
  );
}

export default App;

export const getSVGElement = (content, key) =>
  parse(renderToString(content).replace("<svg", `<svg key="${key}"`));
